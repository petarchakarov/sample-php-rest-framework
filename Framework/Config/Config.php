<?php
namespace Framework\Config;

abstract class Config {
    const DEFAULT_AUTHENTICATOR = '\Framework\Security\Authentication\Authenticators\BasicHTTPAuthenticator';
    /*
     * Since 5.6 we can use const arrays but I'm not sure which version you will use so I will leave it like this
     */
    public static $AUTHENTICATORS = [
            '\Framework\Security\Authentication\Authenticators\BasicHTTPAuthenticator',
            //'\Framework\Security\Authentication\Authenticators\GuestAuthenticator',
    ];
    public static $REQUEST_PROCESSORS = [
            '\Framework\Request\Processor\AuthenticationProcessor',
            '\Framework\Request\Processor\ModuleProcessor',
            '\Framework\Request\Processor\RequestProcessor',
            '\Framework\Request\Processor\ActionProcessor',
            '\Framework\Request\Processor\AuthorizationProcessor',
            '\Framework\Request\Processor\ResponseProcessor'
    ];
    const AUTHORIZATION_PROVIDER = '\Framework\Security\Authorization\Providers\RBACAuthorizationProvider';
}