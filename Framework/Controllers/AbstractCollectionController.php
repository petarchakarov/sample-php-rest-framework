<?php
namespace Framework\Controllers;

use Framework\Models\CollectionModelInterface;
use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Response\Helpers\HTTPStatusCodes;

abstract class AbstractCollectionController extends AbstractController {
    public function __construct(CollectionModelInterface $model) {
        $this->model = $model;
    }
    public function execute(Request $request, Response $response) {
        $result = $this->model->collection($request->getQuery());
        $response->setData($this->appendHrefOnResult($request, $response, $result));
        $response->setStatus(HTTPStatusCodes::HTTP_200_OK);
    }
}