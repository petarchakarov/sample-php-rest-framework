<?php
namespace Framework\Controllers;

use Framework\Models\AbstractModel;
use Framework\Request\Request;
use Framework\Response\Response;

abstract class AbstractController {
    private $model;
    abstract public function execute(Request $request, Response $response);
    protected function appendHrefOnResult(Request $request, Response $response, $result) {
        if(is_array($result)) {
            foreach($result as $r) {
                if(property_exists($r, 'id')) {
                    $r->link = array('href' => $request->getModule()->getURL() . '' . $r->id);
                }
            }
        } else {
            if(property_exists($result, 'id')) {
                $result->link = array('href' => $request->getModule()->getURL() . '' . $result->id);
            }
        }
        return $result;
    }
}