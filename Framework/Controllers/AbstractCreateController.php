<?php
namespace Framework\Controllers;

use Framework\Models\CreateModelInterface;
use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Response\Helpers\HTTPStatusCodes;

abstract class AbstractCreateController extends AbstractController {
    public function __construct(CreateModelInterface $model) {
        $this->model = $model;
    }
    public function execute(Request $request, Response $response) {
        if($this->model->validate($request->getData()) == false) {
            $response->setStatus(HTTPStatusCodes::HTTP_400_BAD_REQUEST);
            $response->setData($this->model->getValidationErrors());
            return null;
        }
        
        $result = $this->model->create($request->getData());
        if($result === false) {
            $response->setStatus(HTTPStatusCodes::HTTP_500_INTERNAL_SERVER_ERROR);
        } else {
            $response->addHeader('Location: ' . $request->getModule()->getURL() . '' . $result->id);
            $response->setData($this->appendHrefOnResult($request, $response, $result));
            $response->setStatus(HTTPStatusCodes::HTTP_201_CREATED);
        }
    }
}