<?php
namespace Framework\Controllers;

use Framework\Models\DeleteModelInterface;
use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Response\Helpers\HTTPStatusCodes;

abstract class AbstractDeleteController extends AbstractController {
    public function __construct(DeleteModelInterface $model) {
        $this->model = $model;
    }
    public function execute(Request $request, Response $response) {
        $this->model->delete($request->getId());
        $response->setStatus(HTTPStatusCodes::HTTP_200_OK);
    }
}