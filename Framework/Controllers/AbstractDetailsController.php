<?php
namespace Framework\Controllers;

use Framework\Models\DetailsModelInterface;
use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Response\Helpers\HTTPStatusCodes;

abstract class AbstractDetailsController extends AbstractController {
    public function __construct(DetailsModelInterface $model) {
        $this->model = $model;
    }
    public function execute(Request $request, Response $response) {
        $result = $this->model->details($request->getId());
        if($result === false) {
            $response->setStatus(HTTPStatusCodes::HTTP_404_NOT_FOUND);
        } else {
            $response->setData($this->appendHrefOnResult($request, $response, $result));
            $response->setStatus(HTTPStatusCodes::HTTP_200_OK);
        }
    }
}