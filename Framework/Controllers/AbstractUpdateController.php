<?php
namespace Framework\Controllers;

use Framework\Models\UpdateModelInterface;
use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Response\Helpers\HTTPStatusCodes;

abstract class AbstractUpdateController extends AbstractController {
    public function __construct(UpdateModelInterface $model) {
        $this->model = $model;
    }
    public function execute(Request $request, Response $response) {
        if($this->model->validate($request->getData()) == false) {
            $response->setStatus(HTTPStatusCodes::HTTP_400_BAD_REQUEST);
            $response->setData($this->model->getValidationErrors());
            return null;
        }
        
        $result = $this->model->update($request->getId(), $request->getData());
        if($result === false) {
            $response->setStatus(HTTPStatusCodes::HTTP_404_NOT_FOUND);
        } else {
            $response->setData($this->appendHrefOnResult($request, $response, $result));
            $response->setStatus(HTTPStatusCodes::HTTP_200_OK);
        }
    }
}