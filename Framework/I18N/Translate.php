<?php
namespace Framework\I18N;

abstract class Translate {
    /*
     * In a real project this class will use gettext library, implement translation files logic, CONFIG values, etc.
     * Now it's a dummy
     */
    public static function gettext($message) {
        return $message;
    }
}