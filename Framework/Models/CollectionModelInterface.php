<?php
namespace Framework\Models;

interface CollectionModelInterface {
    public function collection($query);
}