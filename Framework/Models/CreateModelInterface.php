<?php
namespace Framework\Models;

interface CreateModelInterface extends ValidationModelInterface {
    public function create($input);
}