<?php
namespace Framework\Models;

interface DeleteModelInterface {
    public function delete($id);
}