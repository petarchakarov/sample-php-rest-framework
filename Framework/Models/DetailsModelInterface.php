<?php
namespace Framework\Models;

interface DetailsModelInterface {
    public function details($id);
}