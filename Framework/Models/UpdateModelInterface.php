<?php
namespace Framework\Models;

interface UpdateModelInterface extends ValidationModelInterface {
    public function update($id, $input);
}