<?php
namespace Framework\Models;

interface ValidationModelInterface {
    public function validate($input);
    public function getValidationErrors();
}