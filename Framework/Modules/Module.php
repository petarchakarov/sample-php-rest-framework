<?php
namespace Framework\Modules;

abstract class Module {
    const ACTION_COLLECTION = 1;
    const ACTION_CREATE = 2;
    const ACTION_DELETE = 3;
    const ACTION_DETAILS = 4;
    const ACTION_UPDATE = 5;
    
    private $name;
    /*
     * Here are stored all allowed actions for a module
     */
    protected $actions;
    
    public function __construct($name = '') {
        $this->name = $name;
        $this->actions = [];
    }
    
    public function getController($action) {
        $model = new $this->actions[$action]['model']();
        $controller = new $this->actions[$action]['controller']($model);
        return $controller;
    }
    
    public function getURL() {
        return '/'.strtolower($this->name).'/';
    }
    
    public function isActionSupported($action) {
        if(isset($this->actions[$action])) {
            return true;
        }
        return false;
    }
    
    public function getSupportedActionsString() {
        $actions = array();
        foreach(array_keys($this->actions) as $action) {
            switch($action) {
                case self::ACTION_COLLECTION: $actions[0] = 'GET';break;
                case self::ACTION_DETAILS: $actions[0] = 'GET';break;
                case self::ACTION_CREATE: $actions[1] = 'POST';break;
                case self::ACTION_UPDATE: $actions[2] = 'PUT';break;
                case self::ACTION_DELETE: $actions[3] = 'DELETE';break;
            }
        }
        ksort($actions);
        return implode(', ', array_unique($actions));
    }
    /*
     * Adds new action in a module. If controller and model are not provided, default location is used.
     */
    protected function addAction($action, $controller = null, $model = null) {
        if($controller === null) {
            $controller = '\\Modules\\' . $this->name . '\\Controllers\\' . $this->getDefaultActionClassName($action) . 'Controller';
        }
        if($model === null) {
            $model = '\\Modules\\' . $this->name . '\\Models\\' . $this->getDefaultActionClassName($action) . 'Model';
        }
        $this->actions[$action]['controller'] = $controller;
        $this->actions[$action]['model'] = $model;
    }
    /*
     * @return string Default name for action.
     */
    private function getDefaultActionClassName($action) {
        switch($action) {
            case self::ACTION_COLLECTION:
                return 'Collection';
            case self::ACTION_CREATE:
                return 'Create';
            case self::ACTION_DELETE:
                return 'Delete';
            case self::ACTION_DETAILS:
                return 'Details';
            case self::ACTION_UPDATE:
                return 'Update';
        }
    }
}