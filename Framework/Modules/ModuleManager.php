<?php
namespace Framework\Modules;

use Framework\Exceptions\ModuleNotExistsException;

class ModuleManager {
    /*
     * @throws ModuleNotExistsException if module with such name doesn't exist
     * @return Module Module's implementation
     */
    public function getModule($name) {
        $filename = BASE_DIR . 'Modules/'.ucfirst($name);
        
        if(file_exists($filename) === false) {
            throw new ModuleNotExistsException();
        }
        
        $class = '\\Modules\\' . ucfirst($name) . '\\ModuleInit';
        $module = new $class();
        
        return $module;
    }
}