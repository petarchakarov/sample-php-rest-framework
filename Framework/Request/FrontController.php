<?php
namespace Framework\Request;

use Framework\Request\Processor\AbstractProcessor;
use Framework\Response\Response;

class FrontController {
    private $processorChain;
    private $lastProcessor;
    public function process() {
        $this->processorChain->process(new Request(), new Response());
    }
    public function addProcessor(AbstractProcessor $processor) {
        if($this->processorChain == null) {
            $this->processorChain = $processor;
            $this->lastProcessor = $processor;
            return;
        }
        $this->lastProcessor->setNext($processor);
        $this->lastProcessor = $processor;
    }
}