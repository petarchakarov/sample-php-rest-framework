<?php
namespace Framework\Request\Processor;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Exceptions\ProcessChainException;
use Framework\Response\Helpers\HTTPStatusCodes;

abstract class AbstractProcessor {
    private $next;
    private $request;
    private $response;
    public function setNext(AbstractProcessor $next) {
        $this->next = $next;
    }
    public function process(Request $request, Response $response) {
        $this->request = $request;
        $this->response = $response;
        try {
            $this->handle();
            if ($this->next != null) {
                $this->next->process($this->request, $this->response);
            }
        } catch (ProcessChainException $e) {
            $this->errorResponse();
        }
    }
    private function errorResponse() {
        $this->getResponse()->response();
    }
    protected function getRequest() {
        return $this->request;
    }
    protected function getResponse() {
        return $this->response;
    }
    abstract protected function handle();
}