<?php
namespace Framework\Request\Processor;

use Framework\Request\Processor\Helpers\Route;
use Framework\Request\Processor\Helpers\URIParser;
use Framework\Modules\Module;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Exceptions\ProcessChainException;

class ActionProcessor extends AbstractProcessor {
    protected function handle() {
        $action = $this->getAction();
        if($action === null || $this->getRequest()->getModule()->isActionSupported($action) === false) {
            $this->getResponse()->addHeader('Allow: ' . $this->getRequest()->getModule()->getSupportedActionsString());
            $this->getResponse()->setStatus(HTTPStatusCodes::HTTP_405_METHOD_NOT_ALLOWED);
            throw new ProcessChainException();
        }
        $this->getRequest()->setAction($action);
    }
    private function getAction() {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            return Module::ACTION_CREATE;
        }
        
        if($this->getRequest()->getId() > 0) {
            if($_SERVER['REQUEST_METHOD'] == 'GET') {
                return Module::ACTION_DETAILS;
            }
            if($_SERVER['REQUEST_METHOD'] == 'PUT') {
                return Module::ACTION_UPDATE;
            }
            if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
                return Module::ACTION_DELETE;
            }
            return null;
        }
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            return Module::ACTION_COLLECTION;
        }
        return null;
    }
}
