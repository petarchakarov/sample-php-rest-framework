<?php
namespace Framework\Request\Processor;

use Framework\Security\Authentication\AuthenticationManager;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Exceptions\ProcessChainException;

class AuthenticationProcessor extends AbstractProcessor {
    private $authenticationManager;
    public function __construct(AuthenticationManager $authenticationManager = null) {
        if($authenticationManager === null) {
            $authenticationManager = new AuthenticationManager();
        }
        $this->authenticationManager = $authenticationManager;
    }
    protected function handle() {
    	$this->authenticationManager->authenticate($this->getRequest(), $this->getResponse());

    	if($this->getRequest()->getUser() === null) {
    		$this->authenticationManager->requestAuthentication($this->getResponse());
    		throw new ProcessChainException();
    	}
    }
}
