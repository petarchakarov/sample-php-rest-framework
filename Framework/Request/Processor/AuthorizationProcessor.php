<?php
namespace Framework\Request\Processor;

use Framework\Security\Authorization\AuthorizationProviderManager;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Exceptions\ProcessChainException;

class AuthorizationProcessor extends AbstractProcessor {
    private $authorizationManager;
    public function __construct(AuthorizationProviderManager $authorizationManager = null) {
        if($authorizationManager === null) {
            $authorizationManager = new AuthorizationProviderManager();
        }
        $this->authorizationManager = $authorizationManager;
    }
    protected function handle() {
        $provider = $this->authorizationManager->getProvider();
        if($provider->authorize($this->getRequest()) === false) {
        	$this->getResponse()->setStatus(HTTPStatusCodes::HTTP_403_FORBIDDEN);
        	throw new ProcessChainException();
        }
    }
}