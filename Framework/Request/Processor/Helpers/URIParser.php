<?php
namespace Framework\Request\Processor\Helpers;

class URIParser {
    public static function getModule() {
        $uri = $_SERVER['REQUEST_URI'];
        preg_match('/^\/([a-z0-9]+)/', $uri, $parts);
        if(count($parts) == 2) {
            return $parts[1];
        }
        return null;
    }
    public static function getId() {
        $uri = $_SERVER['REQUEST_URI'];
        preg_match('/^\/([a-z0-9]+)\/?([0-9]+)/', $uri, $parts);
        if(count($parts) == 3) {
            return $parts[2];
        }
        return null;
    }
    public static function getQuery() {
        $uri = $_SERVER['REQUEST_URI'];
        preg_match('/\?.+/', $uri, $parts);
        if(count($parts) != 1) {
            return null;
        }
        $query = trim($parts[0], '?');
        parse_str($query, $out);
        return $out;
    }
}