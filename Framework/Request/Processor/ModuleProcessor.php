<?php
namespace Framework\Request\Processor;

use Framework\Request\Processor\Helpers\URIParser;
use Framework\Modules\ModuleManager;
use Framework\Exceptions\ProcessChainException;
use Framework\Exceptions\ModuleNotExistsException;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Modules\Framework\Modules;

class ModuleProcessor extends AbstractProcessor {
    private $moduleManager;
    public function __construct(ModuleManager $moduleManager = null) {
        if($moduleManager === null) {
            $moduleManager = new ModuleManager();
        }
        $this->moduleManager = $moduleManager;
    }
    protected function handle() {
        $name = URIParser::getModule($_SERVER['REQUEST_URI']);
        if(empty($name)) {
            /*
             * My idea is for a Default module - something like a sitemap which will display all possible modules.
             */
            $name = 'Default';
        }
        try {
            $module = $this->moduleManager->getModule($name);
            $this->getRequest()->setModule($module);
        } catch (ModuleNotExistsException $e) {
            $this->getResponse()->setStatus(HTTPStatusCodes::HTTP_400_BAD_REQUEST);
            throw new ProcessChainException();
        }
    }
}