<?php
namespace Framework\Request\Processor;

use Framework\Request\Processor\Helpers\URIParser;
use Framework\Exceptions\ProcessChainException;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Exceptions\Framework\Exceptions;

class RequestProcessor extends AbstractProcessor {
    protected function handle() {
        try {
            $this->loadData();
            $this->loadId();
            $this->loadQuery();
        } catch (ProcessChainException $e) {
            $this->getResponse()->setStatus(HTTPStatusCodes::HTTP_400_BAD_REQUEST);
            throw $e;
        }
    }
    private function loadData() {
        if($_SERVER['REQUEST_METHOD'] != 'POST' && $_SERVER['REQUEST_METHOD'] != 'PUT') {
            return;
        }
        $body = $this->getRequest()->getBody();
        $json = json_decode($body);
        if($json === null) {
            throw new ProcessChainException();
        }
        $this->getRequest()->setData($json);
    }
    private function loadId() {
        $this->getRequest()->setId(intval(URIParser::getId()));
        if($this->getRequest()->getId() == 0 && ($_SERVER['REQUEST_METHOD'] == 'PUT' || $_SERVER['REQUEST_METHOD'] == 'DELETE')) {
            throw new ProcessChainException();
        }
    }
    private function loadQuery() {
        $this->getRequest()->setQuery(URIParser::getQuery());
    }
}