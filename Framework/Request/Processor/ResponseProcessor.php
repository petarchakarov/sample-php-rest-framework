<?php
namespace Framework\Request\Processor;

class ResponseProcessor extends AbstractProcessor {
    protected function handle() {
    	$controller = $this->getRequest()->getModule()->getController($this->getRequest()->getAction());
        $controller->execute($this->getRequest(), $this->getResponse());
        $this->getResponse()->response();
    }
}