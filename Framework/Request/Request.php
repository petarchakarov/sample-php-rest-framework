<?php
namespace Framework\Request;

use Framework\Controllers\AbstractController;
use Framework\Models\AbstractModel;
use Framework\Modules\Module;
use Framework\Users\User;

class Request {
    private $module;
    private $action;
    private $data;
    private $id;
    private $query;
    private $user;
    public function setModule(Module $module) {
        $this->module = $module;
    }
    public function getModule() {
        return $this->module;
    }
    public function setAction($action) {
        $this->action = $action;
    }
    public function getAction() {
        return $this->action;
    }
    public function setData($data) {
        $this->data = $data;
    }
    public function getData() {
        return $this->data;
    }
    public function setId($id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setQuery($query) {
        $this->query = $query;
    }
    public function getQuery() {
        return $this->query;
    }
    public function setUser(User $user) {
        $this->user = $user;
    }
    public function getUser() {
        return $this->user;
    }
    public function getBody() {
        $body = file_get_contents('php://input');
        return $body;
    }
}