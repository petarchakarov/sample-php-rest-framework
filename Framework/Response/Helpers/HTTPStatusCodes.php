<?php
namespace Framework\Response\Helpers;

abstract class HTTPStatusCodes {
    const HTTP_200_OK = 200;
    const HTTP_201_CREATED = 201;
    const HTTP_204_NO_CONTENT = 204;
    const HTTP_400_BAD_REQUEST = 400;
    const HTTP_401_UNAUTHORIZED = 401;
    const HTTP_403_FORBIDDEN = 403;
    const HTTP_404_NOT_FOUND = 404;
    const HTTP_405_METHOD_NOT_ALLOWED = 405;
    const HTTP_500_INTERNAL_SERVER_ERROR = 500;
    
    public static function getHTTPHeader($status) {
        switch($status) {
            case self::HTTP_200_OK: return 'HTTP/1.1 200 OK';
            case self::HTTP_201_CREATED: return 'HTTP/1.1 201 Created';
            case self::HTTP_204_NO_CONTENT: return 'HTTP/1.1 204 No Content';
            case self::HTTP_400_BAD_REQUEST: return 'HTTP/1.1 400 Bad Request';
            case self::HTTP_401_UNAUTHORIZED: return 'HTTP/1.1 401 Unauthorized';
            case self::HTTP_403_FORBIDDEN: return 'HTTP/1.1 403 Forbidden';
            case self::HTTP_404_NOT_FOUND: return 'HTTP/1.1 404 Not Found';
            case self::HTTP_405_METHOD_NOT_ALLOWED: return 'HTTP/1.1 405 Method Not Allowed';
        }
        return 'HTTP/1.1 500 Internal Server Error';
    } 
}