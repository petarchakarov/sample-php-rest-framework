<?php
namespace Framework\Response;

use Framework\Response\Helpers\HTTPStatusCodes;
class Response {
    private $data;
    private $status;
    private $headers = array();
    public function setData($data) {
        $this->data = $data;
    }
    public function getData() {
        return $this->data;
    }
    public function setStatus($status) {
        $this->status = $status;
    }
    public function getStatus() {
        return $this->status;
    }
    public function addHeader($header) {
        $this->headers[] = $header;
    }
    public function getHeaders() {
        return $this->headers;
    }
    public function response() {
        header('Content-Type: application/json');
        header(HTTPStatusCodes::getHTTPHeader($this->getStatus()));
        foreach($this->getHeaders() as $header) {
            header($header);
        }
        if($this->getData() !== null) {
            if(is_object($this->getData()) || is_array(($this->getData()))) {
                echo json_encode($this->getData());
            } else {
                echo json_encode('{"data": "' . filter_var($this->getData(), FILTER_SANITIZE_MAGIC_QUOTES) . '"}');
            }
        }
    }
}