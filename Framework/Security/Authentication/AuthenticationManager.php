<?php
namespace Framework\Security\Authentication;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Config\Config;

class AuthenticationManager {
    public function authenticate(Request $request, Response $response) {
        /*
         * In a real project here we will populate the authentication chain with all configured authenticators.
         * We can have "anonymous" authenticator for guests.
         */
    	$first = null;
    	$last = null;
    	foreach(Config::$AUTHENTICATORS as $class) {
    	    $auth = new $class();
    		if($first === null) {
    			$first = $auth;
    			$last = $auth;
    			continue;
    		}
    		$last->setNext($auth);
    		$last = $auth;
    	}
        $first->authenticate($request, $response);
    }
    public function requestAuthentication(Response $response) {
        $class = Config::DEFAULT_AUTHENTICATOR;
        $auth = new $class();
        $auth->requestAuthentication($response);
    }
}