<?php
namespace Framework\Security\Authentication\Authenticators;

use Framework\Request\Request;
use Framework\Response\Response;

/*
 * Chain Of Responsibility for authentication - probably we would like multiple authentication options
 */
abstract class AbstractAuthenticator {
    private $request;
    private $response;
    private $next;
    public function setNext(AbstractAuthenticator $next) {
        $this->next = $next;
    }
    public function authenticate(Request $request, Response $response) {
        $this->request = $request;
        $this->response = $response;
        $this->handle();
        if($this->getRequest()->getUser() !== null) {
            // User is authenticated by the previous authenticator
            return;
        }
        if ($this->next != null) {
            $this->next->authenticate($this->request, $this->response);
        }
    }
    protected function getRequest() {
        return $this->request;
    }
    protected function getResponse() {
        return $this->response;
    }
    abstract protected function handle();
    abstract public function requestAuthentication(Response $response);
}