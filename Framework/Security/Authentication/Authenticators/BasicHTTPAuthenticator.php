<?php
namespace Framework\Security\Authentication\Authenticators;

use Framework\Users\UserManager;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Response\Response;

class BasicHTTPAuthenticator extends AbstractAuthenticator {
    /*
     * BASIC HTTP Auth
     * All communication should happen over HTTPS...
     */
    protected function handle() {
        if(isset($_SERVER['PHP_AUTH_USER'])) {
            $user = UserManager::getUser($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
            if($user !== null) {
                $this->getRequest()->setUser($user);
            }
        }
    }
    public function requestAuthentication(Response $response) {
        $response->addHeader('WWW-Authenticate: Basic realm="sample-php-rest-framework"');
        $response->setStatus(HTTPStatusCodes::HTTP_401_UNAUTHORIZED);
        $response->setData("Authentication required.");
    }
}