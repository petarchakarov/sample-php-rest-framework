<?php
namespace Framework\Security\Authentication\Authenticators;

use Framework\Response\Response;
use Framework\Users\User;

class GuestAuthenticator extends AbstractAuthenticator {
    /*
     * Authenticate all
     */
    protected function handle() {
        $this->getRequest()->setUser(new User());
    }
    public function requestAuthentication(Response $response) {
    }
}