<?php
namespace Framework\Security\Authorization;

use Framework\Config\Config;

class AuthorizationProviderManager {
    public function getProvider() {
    	$class = Config::AUTHORIZATION_PROVIDER;
    	return new $class();
    }
}