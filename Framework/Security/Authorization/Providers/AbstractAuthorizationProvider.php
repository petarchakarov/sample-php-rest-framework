<?php
namespace Framework\Security\Authorization\Providers;

use Framework\Request\Request;

abstract class AbstractAuthorizationProvider {
	abstract public function authorize(Request $request);
}