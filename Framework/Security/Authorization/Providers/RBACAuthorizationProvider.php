<?php
namespace Framework\Security\Authorization\Providers;

use Framework\Request\Request;

class RBACAuthorizationProvider extends AbstractAuthorizationProvider {
    public function authorize(Request $request) {
    	/*
    	 * In a real project this provider will check if the current user has permissions to execute the requested action
    	 */
    	return true;
    }
}