<?php
namespace Modules\Contacts\Controllers;

use Framework\Controllers\AbstractCollectionController;

class CollectionController extends AbstractCollectionController {
    public function __construct($model) {
        parent::__construct($model);
    }
}