<?php
namespace Modules\Contacts\Controllers;

use Framework\Controllers\AbstractCreateController;

class CreateController extends AbstractCreateController {
    public function __construct($model) {
        parent::__construct($model);
    }
}