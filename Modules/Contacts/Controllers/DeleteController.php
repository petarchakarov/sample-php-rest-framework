<?php
namespace Modules\Contacts\Controllers;

use Framework\Controllers\AbstractDeleteController;

class DeleteController extends AbstractDeleteController {
    public function __construct($model) {
        parent::__construct($model);
    }
}