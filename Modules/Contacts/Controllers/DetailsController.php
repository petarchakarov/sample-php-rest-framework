<?php
namespace Modules\Contacts\Controllers;

use Framework\Controllers\AbstractDetailsController;

class DetailsController extends AbstractDetailsController {
    public function __construct($model) {
        parent::__construct($model);
    }
}