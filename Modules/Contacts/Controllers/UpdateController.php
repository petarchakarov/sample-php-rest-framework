<?php
namespace Modules\Contacts\Controllers;

use Framework\Controllers\AbstractUpdateController;

class UpdateController extends AbstractUpdateController {
    public function __construct($model) {
        parent::__construct($model);
    }
}