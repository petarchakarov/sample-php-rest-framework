<?php
namespace Modules\Contacts\Models;

use Framework\Models\AbstractModel;
use Framework\Models\CollectionModelInterface;

class CollectionModel extends AbstractModel implements CollectionModelInterface {
    /*
     * In a real project I would use an external library for ORM (it's not allowed to use external libraries for this task).
     * 
     * I consider all other parts in this project as possible for production use.
     * If I try to implement ORM myself it won't be possible to do it right for production use in the timeframe I have.
     * That's why I've decided to isolate all logic here (even not to put it in AbstratModel - depsite the fact this is its purpose).
     * 
     * It's a simple logic, database exceptions will be caught by set_exception_handler in index.php, no transactions, no too complex logic.
     * Object are returned as anonymous (FETCH_OBJ) and I treat them as entities with some properties - for example each "entity" has an id field
     * which is its primary key - and we will use it to create REST links to the resource with this id.
     * 
     * You will have to excuse me but you'll have to change the DB settings in all the five models because I don't want to clutter the code
     * even for DB configurations (the same reason as above). 
     */
    public function collection($query) {
        // We allow searching by keyword query parameter... Just to show this function, no pagination, etc. Simple fetch by criteria.
        $database = new \PDO('mysql:host=localhost;dbname=DBNAME', 'root', '');
        
        $stmt = $database->prepare('SELECT id, name, phone, street FROM contacts ORDER BY id DESC');
        
        /*
         * This fulltext search is the reason why contacts table uses MyISAM. The mysql on my debian machine is <5.6.4 and I've decided
         * there is a chance you would use <5.6.4. As I said in the big comment above I've decided not to use transactions and also don't expect
         * high write loads on this table (because it's not a real project, of course), so we will make it with MyISAM too.
         */
        if(isset($query['keyword'])) {
            $stmt = $database->prepare('SELECT id, name, phone, street FROM contacts WHERE MATCH(name, phone, street) AGAINST (:keyword)');
            $stmt->bindParam(':keyword', $query['keyword']);
        }
        
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_OBJ); //we will return objects - emulating entities in an ORM solution
        return $result;
    }
}