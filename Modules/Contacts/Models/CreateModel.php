<?php
namespace Modules\Contacts\Models;

use Framework\Models\AbstractModel;
use Framework\Models\CreateModelInterface;
use Framework\I18N\Translate;

class CreateModel extends AbstractModel implements CreateModelInterface {
    private $validationError;
    /*
     * In a real project I would use an external library for ORM (it's not allowed to use external libraries for this task).
     * 
     * I consider all other parts in this project as possible for production use.
     * If I try to implement ORM myself it won't be possible to do it right for production use in the timeframe I have.
     * That's why I've decided to isolate all logic here (even not to put it in AbstratModel - depsite the fact this is its purpose).
     * 
     * It's a simple logic, database exceptions will be caught by set_exception_handler in index.php, no transactions, no too complex logic.
     * Object are returned as anonymous (FETCH_OBJ) and I treat them as entities with some properties - for example each "entity" has an id field
     * which is its primary key - and we will use it to create REST links to the resource with this id.
     * 
     * You will have to excuse me but you'll have to change the DB settings in all the five models because I don't want to clutter the code
     * even for DB configurations (the same reason as above). 
     */
    public function create($input) {
        $database = new \PDO('mysql:host=localhost;dbname=DBNAME', 'root', '');
        
        $stmt = $database->prepare('INSERT INTO contacts (name, phone, street) VALUES (:name, :phone, :street)');
        $stmt->bindParam(':name', $input->name, \PDO::PARAM_STR, 15);
        $stmt->bindParam(':phone', $input->phone, \PDO::PARAM_STR, 9); //I use string because it's already validated and we may decide to use numbers starting with 0
        $stmt->bindParam(':street', $input->street, \PDO::PARAM_STR, 45);
        
        $stmt->execute();
        $id = $database->lastInsertId();
        
        $stmt = $database->prepare('SELECT id, name, phone, street FROM contacts WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_OBJ); //we will return objects - emulating entities in an ORM solution
        return $result;
    }
    public function validate($input) {
        if(property_exists($input, 'name') !== true) {
            $this->validationError = Translate::gettext('Name is required.');
            return false;
        }
        if(property_exists($input, 'phone') !== true) {
            $this->validationError = Translate::gettext('Phone number is required.');
            return false;
        }
        if(property_exists($input, 'street') !== true) {
            $this->validationError = Translate::gettext('Street is required.');
            return false;
        }
        /*
         * I've based the validation on the example file (only first names, etc).
         * It's not because I'm so pedantic about the requirements - just have to choose something to do the regexp.
         */
        if(filter_var($input->name, FILTER_VALIDATE_REGEXP, array("options" => array("regexp"=>"/^[A-Z][a-z]{1,14}$/"))) === false) {
            $this->validationError = Translate::gettext('name field validation failed: it must be a capitalized word up to 15 letters.');
            return false;
        }
        if(filter_var($input->phone, FILTER_VALIDATE_REGEXP, array("options" => array("regexp"=>"/^(5|6)[0-9]{8}$/"))) === false) {
            $this->validationError = Translate::gettext('phone field validation failed: it must start with the number 5 or 6 and contain exactly 8 more digits. No spaces allowed.');
            return false;
        }
        if(filter_var($input->street, FILTER_VALIDATE_REGEXP, array("options" => array("regexp"=>'/^[A-Z][a-z]{2,20}(\s[A-Z][a-z]{2,20}){0,1}\s[1-9][0-9]{0,1}$/'))) === false) {
            $this->validationError = Translate::gettext('street field validation failed: it must start with a capitalized word, can be optionally followed by one more capitalized word, and must end with a one or two digit number');
            return false;
        }
        return true;
    }
    public function getValidationErrors() {
        return $this->validationError;
    }
}