<?php
namespace Modules\Contacts\Models;

use Framework\Models\AbstractModel;
use Framework\Models\DetailsModelInterface;

class DetailsModel extends AbstractModel implements DetailsModelInterface {
    /*
     * In a real project I would use an external library for ORM (it's not allowed to use external libraries for this task).
     * 
     * I consider all other parts in this project as possible for production use.
     * If I try to implement ORM myself it won't be possible to do it right for production use in the timeframe I have.
     * That's why I've decided to isolate all logic here (even not to put it in AbstratModel - depsite the fact this is its purpose).
     * 
     * It's a simple logic, database exceptions will be caught by set_exception_handler in index.php, no transactions, no too complex logic.
     * Object are returned as anonymous (FETCH_OBJ) and I treat them as entities with some properties - for example each "entity" has an id field
     * which is its primary key - and we will use it to create REST links to the resource with this id.
     * 
     * You will have to excuse me but you'll have to change the DB settings in all the five models because I don't want to clutter the code
     * even for DB configurations (the same reason as above). 
     */
    public function details($id) {
        $database = new \PDO('mysql:host=localhost;dbname=DBNAME', 'root', '');
        
        $stmt = $database->prepare('SELECT id, name, phone, street FROM contacts WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_OBJ); //we will return objects - emulating entities in an ORM solution
        return $result;
    }
}