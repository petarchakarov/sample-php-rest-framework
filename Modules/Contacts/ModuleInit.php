<?php
namespace Modules\Contacts;

use Framework\Modules\Module;

class ModuleInit extends Module {
    public function __construct() {
        parent::__construct("Contacts");
        $this->addAction(Module::ACTION_COLLECTION);
        $this->addAction(Module::ACTION_CREATE);
        $this->addAction(Module::ACTION_DELETE);
        $this->addAction(Module::ACTION_DETAILS);
        $this->addAction(Module::ACTION_UPDATE);
    }
}