# README #

I've tried to create a PHP REST framework with generic implementation of CRUD actions. As you can see you can write a custom version and you are not obligated to use the generic functions (you can provide your own classes implementing the different interfaces to ModuleInit). I've described important (I thing so) decisions with comments in code. 

This is created as a sample so it's not ready for production use out of the box. One of the main ideas of the task was not to use external libraries.

1. Database schema is located in **docs/schema.sql** (In a real project I will use a migrations library).
2. You have to change DB settings in all the five models at **Modules/Contacts/Models** (it's described why in each model).
3. I've created a bash script which runs a series of CRUD actions located at **docs/curl_usecase.sh**. The output of the script is committed at **curl_usecase_output.txt** for your convenience.
4. You will need PHPUnit for the unit tests. Just run *phpunit .* in the **tests/** directory and all tests will be executed (49 tests, 101 assertions).
5. mod_rewrite is required (I don't consider it external library). Of course it can work (with small changes) without it but we will have very ugly URLs.