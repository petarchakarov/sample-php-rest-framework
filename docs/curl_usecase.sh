#!/bin/bash

function print_header {
    echo -e "\n\n#############################################"
    echo $1
    echo -e $2
    echo -e "#############################################"
}

HOST="http://sample"

# auth required
CMD="curl ${HOST} -i"
print_header "STEP 1 - LOGIN REQUIRED" "$CMD"
eval $CMD

# no contacts
CMD="curl ${HOST}/contacts -i --basic --user petar:123"
print_header "STEP 2 - EMPTY CONTACTS LIST" "$CMD"
eval $CMD

# 404 not found
CMD="curl ${HOST}/contacts/1 -i --basic --user petar:123"
print_header "STEP 3 - GET NOT EXISTING CONTACT" "$CMD"
eval $CMD

# 404 not found
CMD="curl ${HOST}/contacts/1 -X PUT --data '{\"name\": \"Mike\", \"street\": \"Great Street 2\", \"phone\": 581818181}' -i --basic --user petar:123"
print_header "STEP 4 - UPDATE NOT EXISTING CONTACT" "$CMD"
eval $CMD

# error phone is not valid
CMD="curl ${HOST}/contacts/1 -X POST --data '{\"name\": \"Mike\", \"street\": \"Great Street 2\", \"phone\": 481818181}' -i --basic --user petar:123"
print_header "STEP 5 - CREATE CONTACT WITH WRONG PHONE" "$CMD"
eval $CMD

# add contacts
CMD="curl ${HOST}/contacts/1 -X POST --data '{\"name\": \"Ben\", \"street\": \"Cheapside 2\", \"phone\": 581818181}' -i --basic --user petar:123"
print_header "STEP 6.1 - ADD CONTACT" "$CMD"
eval $CMD

CMD="curl ${HOST}/contacts/1 -X POST --data '{\"name\": \"Mike\", \"street\": \"Baker Street 12\", \"phone\": 673748193}' -i --basic --user petar:123"
print_header "STEP 6.2 - ADD CONTACT" "$CMD"
eval $CMD

CMD="curl ${HOST}/contacts/1 -X POST --data '{\"name\": \"Sara\", \"street\": \"Rosebery Avenue 43\", \"phone\": 541010203}' -i --basic --user petar:123"
print_header "STEP 6.3 - ADD CONTACT" "$CMD"
eval $CMD

# list contacts
CMD="curl ${HOST}/contacts -i --basic --user petar:123"
print_header "STEP 7 - LIST CONTACTS" "$CMD"
eval $CMD

# detail view contact
CMD="curl ${HOST}/contacts/1 -i --basic --user petar:123"
print_header "STEP 8 - VIEW CONTACT" "$CMD"
eval $CMD

# update contact
CMD="curl ${HOST}/contacts/1 -X PUT --data '{\"name\": \"Ben\", \"street\": \"Cheapside 22\", \"phone\": 681818181}' -i --basic --user petar:123"
print_header "STEP 1 - UPDATE CONTACT" "$CMD"
eval $CMD

# search by street name
CMD="curl ${HOST}/contacts?keyword=baker -i --basic --user petar:123"
print_header "STEP 1 - SEARCH CONTACTS BY STREET NAME" "$CMD"
eval $CMD

# delete contact
CMD="curl ${HOST}/contacts/1 -X DELETE -i --basic --user petar:123"
print_header "STEP 1 - DELETE CONTACT" "$CMD"
eval $CMD

# detail view contact - not found (deleted in previous step)
CMD="curl ${HOST}/contacts/1 -i --basic --user petar:123"
print_header "STEP 1 - VIEW DELETED CONTACT" "$CMD"
eval $CMD

