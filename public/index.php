<?php
define('BASE_DIR', '../');
require BASE_DIR . 'Framework/Autoload.php';

use Framework\Request\FrontController;
use Framework\Config\Config;

set_exception_handler(function($exception){
    header("HTTP/1.1 500 Internal Server Error");
    exit(0);
});

$fc = new FrontController();
foreach(Config::$REQUEST_PROCESSORS as $class) {
    $processor = new $class();
    $fc->addProcessor($processor);
}
$fc->process();
