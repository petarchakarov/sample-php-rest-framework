<?php 
namespace Tests\Framework\Controllers;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Models\CollectionModelInterface;
use Framework\Models\AbstractModel;
use Framework\Controllers\AbstractCollectionController;
use Framework\Response\Helpers\HTTPStatusCodes;

class CollectionModel extends AbstractModel implements CollectionModelInterface {
    public function collection($query) {
    }
}

class CollectionController extends AbstractCollectionController {
	public function __construct(CollectionModelInterface $model) {
		parent::__construct($model);
	}
}

class AbstractCollectionControllerTest extends \PHPUnit_Framework_TestCase {
    public function test() {
        $query = array('key1' => 'value1', 'key2' => 'value2');
        $result = (object)array('id' => 1);
        
        $response = new Response();
        
        $requestMock = $this->getMockBuilder('Framework\Request\Request')->getMock();
        $requestMock->expects($this->once())
                    ->method('getQuery')
                    ->willReturn($query);
        
        $mockModule = $this->getMockBuilder('Framework\Modules\Module')->getMock();
        $mockModule->expects($this->once())
                    ->method('getURL')
                    ->willReturn('/modulename/');
        
        $requestMock->expects($this->once())
                    ->method('getModule')
                    ->willReturn($mockModule);
        
        $model = $this->getMockBuilder('Tests\Framework\Controllers\CollectionModel')->getMock();
        $model->expects($this->once())
              ->method('collection')
              ->with($this->equalTo($query))
              ->willReturn($result);
        
        $controller = new CollectionController($model);
        $controller->execute($requestMock, $response);
        
        $this->assertEquals($result, $response->getData());
        $this->assertEquals(HTTPStatusCodes::HTTP_200_OK, $response->getStatus());
    }
}