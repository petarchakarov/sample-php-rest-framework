<?php 
namespace Tests\Framework\Controllers;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Models\AbstractModel;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Models\CreateModelInterface;
use Framework\Controllers\AbstractCreateController;

class CreateModel extends AbstractModel implements CreateModelInterface {
    public function create($input) {
    }
    public function validate($input) {
    }
    public function getValidationErrors() {
    }
}

class CreateController extends AbstractCreateController {
	public function __construct(CreateModelInterface $model) {
		parent::__construct($model);
	}
}

class AbstractCreateControllerTest extends \PHPUnit_Framework_TestCase {
    public function test() {
        $entity = array('key1' => 'value1');
        $result = (object)array('id' => 1);
        
        $response = new Response();
        
        $requestMock = $this->getMockBuilder('Framework\Request\Request')->getMock();
        $requestMock->expects($this->exactly(2))
                    ->method('getData')
                    ->willReturn($entity);
        
        $mockModule = $this->getMockBuilder('Framework\Modules\Module')->getMock();
        $mockModule->expects($this->exactly(2))
                    ->method('getURL')
                    ->willReturn('/modulename/');
        
        $requestMock->expects($this->exactly(2))
                    ->method('getModule')
                    ->willReturn($mockModule);
        
        $model = $this->getMockBuilder('Tests\Framework\Controllers\CreateModel')->getMock();
        $model->expects($this->once())
              ->method('validate')
              ->with($this->equalTo($entity))
              ->willReturn(true);
        $model->expects($this->once())
              ->method('create')
              ->with($this->equalTo($entity))
              ->willReturn($result);
        
        $controller = new CreateController($model);
        $controller->execute($requestMock, $response);
        
        $this->assertEquals($result, $response->getData());
        $this->assertEquals(HTTPStatusCodes::HTTP_201_CREATED, $response->getStatus());
    }
    public function testCreateFailed() {
        $entity = array('key1' => 'value1');
        $request = new Request();
        $response = new Response();
        $request->setData($entity);
    
        $model = $this->getMockBuilder('Tests\Framework\Controllers\CreateModel')->getMock();
        $model->expects($this->once())
              ->method('validate')
              ->with($this->equalTo($entity))
              ->willReturn(true);
        $model->expects($this->once())
              ->method('create')
              ->with($this->equalTo($entity))
              ->willReturn(false);
    
        $controller = new CreateController($model);
        $controller->execute($request, $response);
    
        $this->assertEquals(HTTPStatusCodes::HTTP_500_INTERNAL_SERVER_ERROR, $response->getStatus());
    }
    public function testValidationFailed() {
        $entity = array('key1' => 'value1');
        $request = new Request();
        $response = new Response();
        $request->setData($entity);
    
        $model = $this->getMockBuilder('Tests\Framework\Controllers\CreateModel')->getMock();
        $model->expects($this->once())
              ->method('validate')
              ->with($this->equalTo($entity))
              ->willReturn(false);
    
        $controller = new CreateController($model);
        $controller->execute($request, $response);
    
        $this->assertEquals(HTTPStatusCodes::HTTP_400_BAD_REQUEST, $response->getStatus());
    }
}