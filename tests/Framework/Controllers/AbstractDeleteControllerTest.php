<?php 
namespace Tests\Framework\Controllers;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Models\AbstractModel;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Models\DeleteModelInterface;
use Framework\Controllers\AbstractDeleteController;

class DeleteModel extends AbstractModel implements DeleteModelInterface {
    public function delete($id) {
    }
}

class DeleteController extends AbstractDeleteController {
	public function __construct(DeleteModelInterface $model) {
		parent::__construct($model);
	}
}

class AbstractDeleteControllerTest extends \PHPUnit_Framework_TestCase {
    public function test() {
        $id = 101;
        
        $request = new Request();
        $response = new Response();
        $request->setId($id);
        
        $model = $this->getMockBuilder('Tests\Framework\Controllers\DeleteModel')->getMock();
        $model->expects($this->once())
              ->method('delete')
              ->with($this->equalTo($id));
        
        $controller = new DeleteController($model);
        $controller->execute($request, $response);
        
        $this->assertEquals(null, $response->getData());
        $this->assertEquals(HTTPStatusCodes::HTTP_200_OK, $response->getStatus());
    }
}