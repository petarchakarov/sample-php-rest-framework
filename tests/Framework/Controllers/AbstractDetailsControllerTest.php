<?php 
namespace Tests\Framework\Controllers;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Models\AbstractModel;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Models\DetailsModelInterface;
use Framework\Controllers\AbstractDetailsController;

class DetailsModel extends AbstractModel implements DetailsModelInterface {
    public function details($id) {
    }
}

class DetailsController extends AbstractDetailsController {
	public function __construct(DetailsModelInterface $model) {
		parent::__construct($model);
	}
}

class AbstractDetailsControllerTest extends \PHPUnit_Framework_TestCase {
    public function testObjectFound() {
        $id = 101;
        $entity = array('key1' => 'value1');
        
        $request = new Request();
        $response = new Response();
        $request->setId($id);
        
        $model = $this->getMockBuilder('Tests\Framework\Controllers\DetailsModel')->getMock();
        $model->expects($this->once())
              ->method('details')
              ->with($this->equalTo($id))
              ->willReturn($entity);
        
        $controller = new DetailsController($model);
        $controller->execute($request, $response);
        
        $this->assertEquals($entity, $response->getData());
        $this->assertEquals(HTTPStatusCodes::HTTP_200_OK, $response->getStatus());
    }
    public function testObjectNotFound() {
        $id = 101;
    
        $request = new Request();
        $response = new Response();
        $request->setId($id);
    
        $model = $this->getMockBuilder('Tests\Framework\Controllers\DetailsModel')->getMock();
        $model->expects($this->once())
              ->method('details')
              ->with($this->equalTo($id))
              ->willReturn(false);
    
        $controller = new DetailsController($model);
        $controller->execute($request, $response);
    
        $this->assertEquals(null, $response->getData());
        $this->assertEquals(HTTPStatusCodes::HTTP_404_NOT_FOUND, $response->getStatus());
    }
}