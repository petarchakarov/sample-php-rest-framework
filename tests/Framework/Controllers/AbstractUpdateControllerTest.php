<?php 
namespace Tests\Framework\Controllers;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Models\AbstractModel;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Models\UpdateModelInterface;
use Framework\Controllers\AbstractUpdateController;

class UpdateModel extends AbstractModel implements UpdateModelInterface {
    public function update($id, $input) {
    }
    public function validate($input) {
    }
    public function getValidationErrors() {
    }
}

class UpdateController extends AbstractUpdateController {
	public function __construct(UpdateModelInterface $model) {
		parent::__construct($model);
	}
}

class AbstractUpdateControllerTest extends \PHPUnit_Framework_TestCase {
    public function testObjectFound() {
        $id = 101;
        $input = array('key1' => 'value1');
        
        $request = new Request();
        $response = new Response();
        $request->setId($id);
        $request->setData($input);
        
        $model = $this->getMockBuilder('Tests\Framework\Controllers\UpdateModel')->getMock();
        $model->expects($this->once())
              ->method('update')
              ->with($this->equalTo($id), $this->equalTo($input))
              ->willReturn($input);
        $model->expects($this->once())
              ->method('validate')
              ->with($this->equalTo($input))
              ->willReturn(true);
        
        $controller = new UpdateController($model);
        $controller->execute($request, $response);
        
        $this->assertEquals($input, $response->getData());
        $this->assertEquals(HTTPStatusCodes::HTTP_200_OK, $response->getStatus());
    }
    public function testValidationFailed() {
        $id = 101;
        $input = array('key1' => 'value1');
        
        $request = new Request();
        $response = new Response();
        $request->setId($id);
        $request->setData($input);
        
        $model = $this->getMockBuilder('Tests\Framework\Controllers\UpdateModel')->getMock();
        $model->expects($this->once())
              ->method('validate')
              ->with($this->equalTo($input))
              ->willReturn(false);
        
        $controller = new UpdateController($model);
        $controller->execute($request, $response);
        
        $this->assertEquals(null, $response->getData());
        $this->assertEquals(HTTPStatusCodes::HTTP_400_BAD_REQUEST, $response->getStatus());
    }
    public function testUpdateFailed() {
        $id = 101;
        $input = array('key1' => 'value1');
    
        $request = new Request();
        $response = new Response();
        $request->setId($id);
        $request->setData($input);
    
        $model = $this->getMockBuilder('Tests\Framework\Controllers\UpdateModel')->getMock();
        $model->expects($this->once())
              ->method('validate')
              ->with($this->equalTo($input))
              ->willReturn(true);
        $model->expects($this->once())
              ->method('update')
              ->with($this->equalTo($id), $this->equalTo($input))
              ->willReturn(false);
    
        $controller = new UpdateController($model);
        $controller->execute($request, $response);
    
        $this->assertEquals(null, $response->getData());
        $this->assertEquals(HTTPStatusCodes::HTTP_404_NOT_FOUND, $response->getStatus());
    }
}