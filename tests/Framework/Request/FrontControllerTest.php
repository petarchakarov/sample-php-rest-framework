<?php 
namespace Tests\Framework\Request;

use Framework\Request\Processor\AbstractProcessor;
use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Request\FrontController;

class MockProcessor extends AbstractProcessor {
    protected function handle() {
        echo "something";
    }
}

class FrontControllerTest extends \PHPUnit_Framework_TestCase {
    public function test() {
        $mockProcessor = $this->getMockBuilder('Tests\Framework\Request\MockProcessor')->getMock();
        $mockProcessor->expects($this->once())
              ->method('process')
              ->with(
                      $this->callback(function($request){
                          if(get_class($request) == 'Framework\Request\Request') {
                              return true;
                          } else {
                              return false;
                          }
                      }
                      ),
                      $this->callback(function($response){
                          if(get_class($response) == 'Framework\Response\Response') {
                              return true;
                          } else {
                              return false;
                          }
                      })
                );
        $fc = new FrontController();
        $fc->addProcessor($mockProcessor);
        $fc->process();
    }
}