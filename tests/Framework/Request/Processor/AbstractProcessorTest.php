<?php 
namespace Tests\Framework\Request\Process;

use Framework\Request\Processor\AbstractProcessor;
use Framework\Request\Request;
use Framework\Response\Response;

class MockProcessor1 extends AbstractProcessor {
    protected function handle() {
        $this->getResponse()->setData("123");
    }
}

class MockProcessor2 extends AbstractProcessor {
    protected function handle() {
        $this->getResponse()->setData($this->getResponse()->getData() . "456");
    }
}

class AbstractProcessorTest extends \PHPUnit_Framework_TestCase {
    public function test() {
        $data = "123456";
        $request = new Request();
        $response = new Response();
        
        $processor1 = new MockProcessor1();
        $processor2 = new MockProcessor2();
        $processor1->setNext($processor2);
        $processor1->process($request, $response);
        $this->assertEquals($data, $response->getData());
    }
}