<?php 
namespace Tests\Framework\Request\Process;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Request\Processor\ActionProcessor;
use Framework\Modules\Module;
use Framework\Response\Helpers\HTTPStatusCodes;

class ActionProcessorTest extends \PHPUnit_Framework_TestCase {
    public function actionsProvider() {
        return array(
            array('GET', Module::ACTION_COLLECTION, null),
            array('POST', Module::ACTION_CREATE, null),
            array('DELETE', Module::ACTION_DELETE, 5),
            array('GET', Module::ACTION_DETAILS, 5),
            array('PUT', Module::ACTION_UPDATE, 5),
        );
    }
    /**
     * @dataProvider actionsProvider
     */
    public function testActionNotSupported($method, $action, $id) {
        $request = new Request();
        
        $_SERVER['REQUEST_METHOD'] = $method;
        
        $responseMock = $this->getMockBuilder('Framework\Response\Response')->getMock();
        $responseMock->expects($this->once())
                     ->method('setStatus')
                     ->with($this->equalTo(HTTPStatusCodes::HTTP_405_METHOD_NOT_ALLOWED));
        $responseMock->expects($this->once())
                     ->method('response');
        
        $mockModule = $this->getMockBuilder('Framework\Modules\Module')->getMock();
        $mockModule->expects($this->once())
                   ->method('isActionSupported')
                   ->with($this->equalTo($action))
                   ->willReturn(false);
        
        $request->setModule($mockModule);
        $request->setId($id);
        
        $processor = new ActionProcessor();
        
        $processor->process($request, $responseMock);
    }
    /**
     * @dataProvider actionsProvider
     */
    public function testActionSupported($method, $action, $id) {
        $request = new Request();
        $response = new Response();
    
        $_SERVER['REQUEST_METHOD'] = $method;
        
        $mockModule = $this->getMockBuilder('Framework\Modules\Module')->getMock();
        $mockModule->expects($this->once())
                   ->method('isActionSupported')
                   ->with($this->equalTo($action))
                   ->willReturn(true);
        
        $request->setModule($mockModule);
        $request->setId($id);
    
        $processor = new ActionProcessor();
    
        $processor->process($request, $response);
        $this->assertEquals($action, $request->getAction());
    }
}