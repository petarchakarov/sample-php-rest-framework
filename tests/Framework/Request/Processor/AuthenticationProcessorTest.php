<?php 
namespace Tests\Framework\Request\Process;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Request\Processor\AuthenticationProcessor;
use Framework\Users\User;

class AuthenticationProcessorTest extends \PHPUnit_Framework_TestCase {
    public function testAuthenticationSuccessful() {
        $response = new Response();
        
        $requestMock = $this->getMockBuilder('Framework\Request\Request')->getMock();
        $requestMock->expects($this->once())
                    ->method('getUser')
                    ->willReturn(new User());
        
        $authManagerMock = $this->getMockBuilder('Framework\Security\Authentication\AuthenticationManager')->getMock();
        $authManagerMock->expects($this->once())
                        ->method('authenticate')
                        ->with($this->identicalTo($requestMock), $this->identicalTo($response));
        
        $processor = new AuthenticationProcessor($authManagerMock);
        $processor->process($requestMock, $response);
    }
    public function testAuthenticationFailed() {
        $request = new Request();
        
        $responseMock = $this->getMockBuilder('Framework\Response\Response')->getMock();
        $responseMock->expects($this->once())
                     ->method('response');
    
        $authManagerMock = $this->getMockBuilder('Framework\Security\Authentication\AuthenticationManager')->getMock();
        $authManagerMock->expects($this->once())
                        ->method('authenticate')
                        ->with($this->identicalTo($request), $this->identicalTo($responseMock));
        $authManagerMock->expects($this->once())
                        ->method('requestAuthentication')
                        ->with($this->identicalTo($responseMock));
        
        
    
        $processor = new AuthenticationProcessor($authManagerMock);
        $processor->process($request, $responseMock);
    }
}