<?php 
namespace Tests\Framework\Request\Process;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Users\User;
use Framework\Request\Processor\AuthorizationProcessor;
use Framework\Response\Helpers\HTTPStatusCodes;

class AuthorizationProcessorTest extends \PHPUnit_Framework_TestCase {
    public function testAuthorizationSuccessful() {
        $request = new Request();
        $response = new Response();
        
        $authProvider = $this->getMockForAbstractClass('Framework\Security\Authorization\Providers\AbstractAuthorizationProvider');
        $authProvider->expects($this->once())
                     ->method('authorize')
                     ->with($this->identicalTo($request))
                     ->willReturn(true);
        
        $authManagerMock = $this->getMockBuilder('Framework\Security\Authorization\AuthorizationProviderManager')->getMock();
        $authManagerMock->expects($this->once())
                        ->method('getProvider')
                        ->willReturn($authProvider);
        
        $processor = new AuthorizationProcessor($authManagerMock);
        $processor->process($request, $response);
    }
    public function testAuthorizationFailed() {
        $request = new Request();
        
        $responseMock = $this->getMockBuilder('Framework\Response\Response')->getMock();
        $responseMock->expects($this->once())
                     ->method('setStatus')
                     ->with($this->equalTo(HTTPStatusCodes::HTTP_403_FORBIDDEN));
        $responseMock->expects($this->once())
                     ->method('response');
        
        $authProvider = $this->getMockForAbstractClass('Framework\Security\Authorization\Providers\AbstractAuthorizationProvider');
        $authProvider->expects($this->once())
                     ->method('authorize')
                     ->with($this->identicalTo($request))
                     ->willReturn(false);
        
        $authManagerMock = $this->getMockBuilder('Framework\Security\Authorization\AuthorizationProviderManager')->getMock();
        $authManagerMock->expects($this->once())
                        ->method('getProvider')
                        ->willReturn($authProvider);
        
        $processor = new AuthorizationProcessor($authManagerMock);
        $processor->process($request, $responseMock);
    }
}