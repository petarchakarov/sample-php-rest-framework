<?php 
namespace Tests\Framework\Request\Processor\Helpers;

use Framework\Request\Processor\Helpers\URIParser;

class URIParserTest extends \PHPUnit_Framework_TestCase {
    public function testGetModule() {
        $_SERVER['REQUEST_URI'] = '/contacts';
        $this->assertEquals('contacts', URIParser::getModule());
        $_SERVER['REQUEST_URI'] = '/contacts-aa';
        $this->assertEquals('contacts', URIParser::getModule());
        $_SERVER['REQUEST_URI'] = '/contacts/100';
        $this->assertEquals('contacts', URIParser::getModule());
        $_SERVER['REQUEST_URI'] = '/contacts?key1=value1';
        $this->assertEquals('contacts', URIParser::getModule());
    }
    public function testGetId() {
        $_SERVER['REQUEST_URI'] = '/contacts/25';
        $this->assertEquals(25, URIParser::getId());
        $_SERVER['REQUEST_URI'] = '/contacts';
        $this->assertEquals(null, URIParser::getId());
    }
    public function testGetQuery() {
        $_SERVER['REQUEST_URI'] = '/contacts?key1=value1';
        $this->assertEquals(array('key1' => 'value1'), URIParser::getQuery());
        $_SERVER['REQUEST_URI'] = '/contacts';
        $this->assertEquals(null, URIParser::getQuery());
        $_SERVER['REQUEST_URI'] = '/contacts/100';
        $this->assertEquals(null, URIParser::getQuery());
    }
}