<?php 
namespace Tests\Framework\Request\Process;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Request\Processor\ModuleProcessor;
use Framework\Exceptions\ModuleNotExistsException;

class ModuleProcessorTest extends \PHPUnit_Framework_TestCase {
    public function testModuleExists() {
        $request = new Request();
        $response = new Response();
        
        $_SERVER['REQUEST_URI'] = '/modulename/';
        
        $mockModule = $this->getMockForAbstractClass('Framework\Modules\Module');
        
        $moduleManager = $this->getMockBuilder('Framework\Modules\ModuleManager')->getMock();
        $moduleManager->expects($this->once())
                        ->method('getModule')
                        ->with($this->equalTo('modulename'))
                        ->willReturn($mockModule);
        
        $processor = new ModuleProcessor($moduleManager);
        $processor->process($request, $response);
    }
    public function testModuleNotExists() {
        $request = new Request();
        
        $_SERVER['REQUEST_URI'] = '/modulename/';
        
        $responseMock = $this->getMockBuilder('Framework\Response\Response')->getMock();
        $responseMock->expects($this->once())
                     ->method('setStatus')
                     ->with($this->equalTo(HTTPStatusCodes::HTTP_400_BAD_REQUEST));
        $responseMock->expects($this->once())
                     ->method('response');
        $moduleManager = $this->getMockBuilder('Framework\Modules\ModuleManager')->getMock();
        $moduleManager->expects($this->once())
                        ->method('getModule')
                        ->with($this->equalTo('modulename'))
                        ->will($this->throwException(new ModuleNotExistsException()));
        
        $processor = new ModuleProcessor($moduleManager);
        $processor->process($request, $responseMock);
    }
}