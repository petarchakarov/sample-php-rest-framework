<?php 
namespace Tests\Framework\Request\Process;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Response\Helpers\HTTPStatusCodes;
use Framework\Request\Processor\RequestProcessor;

class RequestProcessorTest extends \PHPUnit_Framework_TestCase {
    public function createAndUpdateProvider() {
        return array(
                array('POST'),
                array('PUT')
        );
    }
    public function detailsAndDeleteProvider() {
        return array(
                array('GET'),
                array('DELETE')
        );
    }
    /**
     * @dataProvider createAndUpdateProvider
     */
    public function testCreateAndUpdateWithoutData($method) {
        $_SERVER['REQUEST_METHOD'] = $method;
        
        $responseMock = $this->getMockBuilder('Framework\Response\Response')->getMock();
        $responseMock->expects($this->once())
                     ->method('setStatus')
                     ->with($this->equalTo(HTTPStatusCodes::HTTP_400_BAD_REQUEST));
        $responseMock->expects($this->once())
                     ->method('response');
        
        $requestMock = $this->getMockBuilder('Framework\Request\Request')->getMock();
        $requestMock->expects($this->once())
                    ->method('getBody')
                    ->willReturn('<not_quite_json>');
        
        $processor = new RequestProcessor();
        $processor->process($requestMock, $responseMock);
    }
    public function testUpdateWithoutId() {
        $_SERVER['REQUEST_METHOD'] = 'PUT';
        $_SERVER['REQUEST_URI'] = '/modulename/';
    
        $responseMock = $this->getMockBuilder('Framework\Response\Response')->getMock();
        $responseMock->expects($this->once())
                     ->method('setStatus')
                     ->with($this->equalTo(HTTPStatusCodes::HTTP_400_BAD_REQUEST));
        $responseMock->expects($this->once())
                     ->method('response');
    
        $requestMock = $this->getMockBuilder('Framework\Request\Request')->getMock();
        $requestMock->expects($this->once())
                    ->method('getBody')
                    ->willReturn('{"test": 1}');
        $requestMock->expects($this->once())
                    ->method('getId')
                    ->willReturn(0);
    
        $processor = new RequestProcessor();
        $processor->process($requestMock, $responseMock);
    }
    public function testDeleteWithoutId() {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';
        $_SERVER['REQUEST_URI'] = '/modulename/';
    
        $responseMock = $this->getMockBuilder('Framework\Response\Response')->getMock();
        $responseMock->expects($this->once())
                     ->method('setStatus')
                     ->with($this->equalTo(HTTPStatusCodes::HTTP_400_BAD_REQUEST));
        $responseMock->expects($this->once())
                     ->method('response');
        
        $requestMock = $this->getMockBuilder('Framework\Request\Request')->getMock();
        $requestMock->expects($this->once())
                    ->method('getId')
                    ->willReturn(0);
    
        $processor = new RequestProcessor();
        $processor->process($requestMock, $responseMock);
    }
    /**
     * @dataProvider createAndUpdateProvider
     */
    public function testCreateAndUpdate($method) {
        $response = new Response();
        $_SERVER['REQUEST_METHOD'] = $method;
        $_SERVER['REQUEST_URI'] = '/modulename/25';
        $body = '{"test": 1}';
    
        $requestMock = $this->getMockBuilder('Framework\Request\Request')->getMock();
        $requestMock->expects($this->once())
                    ->method('getBody')
                    ->willReturn($body);
        $requestMock->expects($this->once())
                    ->method('getId')
                    ->willReturn(25);
        $requestMock->expects($this->once())
                    ->method('setId')
                    ->with($this->equalTo(25));
        $requestMock->expects($this->once())
                    ->method('setData')
                    ->with(json_decode($body));
    
        $processor = new RequestProcessor();
        $processor->process($requestMock, $response);
    }
    /**
     * @dataProvider detailsAndDeleteProvider
     */
    public function testDetailsAndDeleteWithId($method) {
        $response = new Response();
        $_SERVER['REQUEST_METHOD'] = $method;
        $_SERVER['REQUEST_URI'] = '/modulename/25';
    
        $requestMock = $this->getMockBuilder('Framework\Request\Request')->getMock();
        $requestMock->expects($this->once())
                    ->method('getId')
                    ->willReturn(25);
        $requestMock->expects($this->once())
                    ->method('setId')
                    ->with($this->equalTo(25));
    
        $processor = new RequestProcessor();
        $processor->process($requestMock, $response);
    }
    public function testCollectionWithQuery() {
        $request = new Request();
        $response = new Response();
        
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $_SERVER['REQUEST_URI'] = '/modulename?key1=value1&key2=value2';
    
        $processor = new RequestProcessor();
        $processor->process($request, $response);
        $this->assertEquals(array('key1' => 'value1', 'key2' => 'value2'), $request->getQuery());
    }
}