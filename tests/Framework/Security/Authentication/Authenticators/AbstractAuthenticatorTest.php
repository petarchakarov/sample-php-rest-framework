<?php 
namespace Tests\Framework\Request\Process;

use Framework\Request\Request;
use Framework\Response\Response;
use Framework\Security\Authentication\Authenticators\AbstractAuthenticator;

class MockAuthenticator1 extends AbstractAuthenticator {
    protected function handle() {
        $this->getResponse()->setData("123");
    }
    public function requestAuthentication(Response $response) {
    }
}

class MockAuthenticator2 extends AbstractAuthenticator {
    protected function handle() {
        $this->getResponse()->setData($this->getResponse()->getData() . "456");
    }
    public function requestAuthentication(Response $response) {
    }
}

class AbstractAuthenticatorTest extends \PHPUnit_Framework_TestCase {
    public function test() {
        $data = "123456";
        $request = new Request();
        $response = new Response();
        
        $authenticator1 = new MockAuthenticator1();
        $authenticator2 = new MockAuthenticator2();
        $authenticator1->setNext($authenticator2);
        $authenticator1->authenticate($request, $response);
        $this->assertEquals($data, $response->getData());
    }
}