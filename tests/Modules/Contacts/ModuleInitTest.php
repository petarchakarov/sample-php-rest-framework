<?php 
namespace Tests\Modules\Contacts;

use Modules\Contacts\ModuleInit;
use Framework\Modules\Module;

class ModuleInitTest extends \PHPUnit_Framework_TestCase {
    public function actionsProvider() {
        return array(
                array(Module::ACTION_COLLECTION),
                array(Module::ACTION_CREATE),
                array(Module::ACTION_DELETE),
                array(Module::ACTION_DETAILS),
                array(Module::ACTION_UPDATE),
        );
    }
    /**
     * @dataProvider actionsProvider
     */
    public function testIsActionSupported($action) {
        $module = new ModuleInit();
        $this->assertEquals(true, $module->isActionSupported($action));
    }
    public function testControllerClasses() {
        $module = new ModuleInit();
        $this->assertEquals('Modules\Contacts\Controllers\CollectionController', get_class($module->getController(Module::ACTION_COLLECTION)));
        $this->assertEquals('Modules\Contacts\Controllers\CreateController', get_class($module->getController(Module::ACTION_CREATE)));
        $this->assertEquals('Modules\Contacts\Controllers\DeleteController', get_class($module->getController(Module::ACTION_DELETE)));
        $this->assertEquals('Modules\Contacts\Controllers\DetailsController', get_class($module->getController(Module::ACTION_DETAILS)));
        $this->assertEquals('Modules\Contacts\Controllers\UpdateController', get_class($module->getController(Module::ACTION_UPDATE)));
    }
    public function testGetSupportedActions() {
        $module = new ModuleInit();
        $this->assertEquals('GET, POST, PUT, DELETE', $module->getSupportedActionsString());
    }
    public function testGetURL() {
        $module = new ModuleInit();
        $this->assertEquals('/contacts/', $module->getURL());
    }
}